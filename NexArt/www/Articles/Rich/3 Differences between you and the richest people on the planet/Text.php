<h1 style="text-align: center; line-height: 1.8em;">3 отличия между вами и самыми богатыми людьми планеты.
Накапливайте. Используйте. Сберегайте.</h1>
</br></br></br></br>
<div class="article_text">
<h2>Ошибка, которую совершают все</h2>

  <p style="margin: 3% 0 0 3%;">Мы тратим <span>11 лет нашей жизни</span> на школу, в надежде добиться успеха. Получить одобрение от родителей. Расположить к себе любимую девушку. <span>После школы мы вдуг осознаем</span>, что все наши знания стоят чуть больше того мела, которым мы на протяжении всей жизни, пачкали школьную доску.<p>
	  
  <p style="margin: 3% 0 0 3%;">И вот перед нами появляется возможность. Институт. Взрослая жизнь. Наконец - то я смогу показать себя. Показать чего я стою. И конечно же награда не заставит себя долго ждать. У меня будет работа, которая доставляет мне удовольствие. Да и высокое жалование, чего еще можно желать ?!</p>

<p style="margin: 3% 0 0 3%;">Так мы думаем и всеми силами, стараемся достич нашей заветной мечты: опять контрольная, уже которая по счету сессия, и мы даже начинаем верить , что мы стали умнее. <span class="important_text">Вспомните это чувство.</span> Когда вас ставят в пример, перед вашими друзьями. Одногрупниками. Коллегами. Это все прекрасно. А теперь представьте ситуацию. Вы генеральный директор компании “N”. В вашем штате огромное число выдающихся сотрудников. У каждого из которых десятилетний опыт за плечами. <span class="important_text">Станите ли вы брать на работу и платить свои деньги человеку</span>, который способен с блеском только выполнять домашнее задание ? Если ответ ДА, то смело закрывайте страницу браузера и возвращайтесь к вашим повседневным заботам. Ведь это ответ лицемеров и лжецов. И с теми и с другими у меня нету желания продолжать разговор.</p>

<p style="margin: 3% 0 0 3%;">Если же вы относитесь к остальной группе людей, то наверняка вы начали задаваться вопросом,<span> что будет дальше после института.</span> Быть может вы уже работали или пытались устроится работать по профессии, после чего осознали на сколько это тяжело найти высокооплачиваемую, работу 22 - х летнему студенту. Одним работадателям нужен ОПЫТ. Другим не нравится как вы одеваетесь. Третьим как говорите и еще 1000 причин почему вы не подходите. В итоге вы решаете, как же хорошо было в институте, может пойти в Магистратуру<span>( И потерять еще пару лет жизни ).</span> Зато наберешься опыта, после чего станешь большим специалистом в своей области.</p>

<p style="margin: 3% 0 0 3%;"><span class="important_text">Так говорили все мои учителя.</span> И я восхищался их взглядами на мир, ведь они действительно, знали больше меня( Учителя как ни как ). Думаю каждый из нас задавался вопросом : Почему, все учителя, которых мы встречали по жизни, хоть и являются прекрасными специалистами. Зарабатывают по 30 тыс руб. в месяц. Ведь даже работая на продавцом на рынке без высшего образования.<span class="important_text"> Можно зарабатывать больше</span> .</p>

<p style="margin: 3% 0 0 3%;">Все потому, что <span>человек достигает совершенства в том, чем он занимается</span>, в том куда он вкладывает свою душу. Учителя всю жизнь учаться обучать своих студентов. Совершенствуются в своих предметах, пытаются узнать что то новое, стать лучшим в своей области. Так и происходит. Преподаватели всё знают по своему предмету. Но кто из них изучал предмет “Деньги”? Ответ один - никто. Поэтому они ничего не знают, о том как зарабатывать много. Все их представление о деньгах заканчивается на слухах своих родителей и друзей. А ведь все просто. <span>Что бы начать зарабатывать много денег.</span> Нужно учиться управлять деньгами. Как правильно ими нужно пользоваться. Преумножать их количество.</p>

</br></br></br></br>
<h2>Экономика - искусство экономить</h2>

<p style="margin: 3% 0 0 3%;">Думаю, я не открыл перед вами вселенную в прошлом абзаце. Все мы так или иначе осознавали это. Но как изучить эту науку - “Деньги”. Да и что вообще надо знать ? У кого учиться этому ремеслу ?
Пожалуй самый верный ответ у тех, кто уже сумел разбогатеть. Уорен Баффет - миллиардер из США, до сих пор живет в скромном домике, который он приобрел в середине прошлого 
100 - летия. Дональд Трамп - всегда торгуется до последнего, чтобы купить товар по самой низкой цене. Они умеют ЭКОНОМИТЬ деньги. Богатые всегда покупают вещи со скидкой и не позволяют себе тратить больше чем нужно. В этом 1-й секрет богатства. Вы должны научить накапливать ваш капитал.</p>


</br></br></br></br>
<h2 style="line-height: 1.3em;">Увеличить или потратить - </br>
правильное использование денег</h2>

<p style="margin: 3% 0 0 3%;">Ну и что. Скажите вы мне, я умею копить деньги, за всю свою жизнь я смог скопить на 2 новенькие машины. Дачу за городом, а также собственный дом. Но при этом я не прикуриваю стодолларовыми банкнотами сигары. Так и есть. И даже больше вы никогда не достигните таких вершин, если продолжите действовать в том же ритме, что и сейчас. Вы будете накапливать деньги, а после этого благополучно тратить их на разные безделушки, которые вы посчитаете просто необходимыми для покупки именно сейчас, ведь как же я обойдусь без 10-й пары этих модных джинс. Да они же еще и по скидке. Ну все, я беру две.</p>

<p style="margin: 3% 0 0 3%;">Поэтому вторым пунктом на пути к вашим золотым горам будет научиться использовать накопленные вами деньги для увеличения их количества. Обратите внимание на успешных бизнесменов. Все они ходят в совершенно обычной, ничем ни примечательной одежде. Рубашка. Галстук. Брюки. В сумме получаем костюм за 2 - 3 тысячи рублей. И при этом цена их компании может исчесляться миллионами. Почему же спросите вы ? Все потому, что они используют свои деньги, что бы заработать еще больше денег. Ну и еще потому что не очень то и удобно, ходить постоянно в черном костюме за 100 000 тысяч в 30 - ти градусную жару, под палящим солнцем. 
И так подведем итог. Второе правило гласит. После того как вам удалось накопить денег, вы должны использовать их для преумножения вашего состояния.</p>


</br></br></br></br>
<h2>Сохранить легчем чем заработать</h2>

<p style="margin: 3% 0 0 3%;">Предположим вам каким то чудом удалось найти способ, для умножения ваших денег. 
И вот вы - господин жизни сидите, на своей горе золота и думаете “жизнь удалась”. Я достиг всего о чем только мечтал. Но вот незадача к вам в квартиру стучиться сначала налоговый инспектор, потом ваши работники, которым задолжали зарплату. Вместе с ними приходит налоговый инспектор со своими друзьями кредиторами. И вот вы уже сидите в пустой квартире, а в ваш тупой взгляд уставлен на единственную оставшуюся монету на дне вашей драгоценной копилки. Где же мои деньги ? Где мои честно нажитые монетки ? Ради чего я трудился не покладая рук весь этот месяц. Неужели ради этого единственного оставшегося рубля ?</p>

<p style="margin: 3% 0 0 3%;">Наверное каждый из нас хоть раз слышал случаи, про то как счастливчик, выигрывает в лотерею 50 млн. долларов, а уже через пол года от выигрыша остаются только старые статьи в газетах. Такая же участь достаётся и хозяину. Все потому, что он не знает как правильно сохранять накопленное состояние. Таким образом мы плавно переходим к третьему закону, богатых людей : Научитесь сохранять ваше богатство. Да именно так, спросите любого предпринемателя что легче заработать деньги или сохранить уже заработанные. Уверен он выберет второй вариант. Это очень просто. Пройдитесь лишний раз пешком, вместо езды на автомобиле. Откажитесь от покупки очередного iphone. И уже через пару месяцев вы заметите, что монеты на вашем банковском счету, обзавелись тысячами новых друзей. Вам всего то нужно экономить каждый день по чуть - чуть. Вы даже и не заметите этого. Зато ваш кошелек заметит.</p>

<p style="margin: 3% 0 0 3%;">Как же много здесь воды. Ни одного конкретного слова или руководства к действию!
Не беспокойся. Самое интересное впереди.
А пока ты узнал необходимое, чтобы выделится из общей массы: “Работников”. </p>

<p style="margin: 3% 0 0 3%; font-weight: bold;">Перед тем как вы пошли к другим мои статьям. Еще раз вспомним 3 этапа на пути к вашей жизни в пентхаусе небоскреба:</p>

<p style="margin: 3% 0 0 3%;">1)Научитесь накапливать деньги.</p>
<p style="margin: 3% 0 0 3%;">2)Научитесь использовать деньги, чтобы преумножить ваше состояние.</p>
<p style="margin: 3% 0 0 3%;">3)Научитесь сохранять баше богатство.</p>
</br></br>
<p style="margin: 3% 0 0 3%;">Можете относится ко всему, что сдесь написано с долей скептицизма. Но это работает, хотите вы того или нет.
Ну что же прощайте.…………………….  Или нет ?</p>
</div>