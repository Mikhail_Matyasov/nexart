
function GetAverage(x,y){
	"use strict";
	return (x + y) / 2;
}

$(document).ready(function(){
	"use strict";
	
	var min = GetAverage(screen.height,screen.width);
 	document.getElementsByTagName( "body" )[0].style[ "font-size" ] = min/80 + "px";
	
	var $menu = $(".bar-container").children();
	var $cross = $(".container_menu .fa-times");
	
	$menu.on("click",function(){
		$(".container_menu").css({display:"block"});
	});
	$cross.on("click",function(){
		$(".container_menu").css({display:"none"});
	});
	
	$(window).resize(function(){
		var mql = window.matchMedia("(orientation: portrait)");

		if(!mql.matches) {  
			$(".container_menu").css({display:"none"});
		}
		
	});
});
