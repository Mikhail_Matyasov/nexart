$(document).ready(function(){
	"use strict";
	$.post("../PHP/DeleteCookie.php");
	
	function getRandomNumber(min,max){
		var dif = max - min;
		return parseInt(((Math.random() * 100) % dif) + min);
	} 
	//--------------------------------------------------
	function ResizeTextarea($textarea){
		var lenght = $textarea.text().length;
		var cols;
		
		if(lenght < 100){
			cols = getRandomNumber(20,30);
		} 
		else if(lenght >= 100 && lenght <= 300){
			cols = getRandomNumber(30,60);
		}
		else{
			cols = getRandomNumber(60,75);
		}
		
		var rows = Math.ceil(lenght / cols) + 1;
		$textarea.attr("cols",cols); 
		$textarea.attr("rows",rows);
	}
	
	function ResizeSpan($span){
		if($span.width() < 70){
			$span.css({
				width:"40%",
				margin: "0 0% 0 60%"
			});
		}
		
		var name = $span.text();
		
		if(name.length > 6){
			
			var sub = name.substring(0,6);
			$span.text(sub + "..");
		}
	}
	
	function AddMargin($textarea,$block){
			$block.css({
				width: $block.width()*1.03
			});
			
			$textarea.css({
				marginRight: "3%"
			});
	}
	$("#reviews textarea").each(function(){
		ResizeTextarea($(this));
		ResizeSpan($(this).siblings("span"));
		AddMargin($(this),$(this).parent());
	});
	//--------------------------------------------------
	$("#sendNewReview .reviewBlock i").click(function(){
		var $siblings = $(this).siblings();
		$siblings.val("");
		$siblings.focus();
	});
	
	//--------------------------------------------------
	
	$("#sendNewReview form input[type='button']").click(function(){
		var $nameInput = $("#sendNewReview form input[name='name']");
		var $emailInput = $("#sendNewReview form input[name='email']");
		var $textarea = $("#sendNewReview form textarea");
		var name = $nameInput.val();
		var email = $emailInput.val();
		var text = $textarea.val();
		if(text.length > 0){
			
		
		$.post("../PHP/SendReview.php",
			  {
				name: name,
				email: email,
				text: text
			  },function(){
			$textarea.val("");
			
			var $newReviewBlock = $("<div class='reviewBlock'></div>");
			var $newTextarea = $("<textarea readonly='readonly' cols='20' rows='4'  class='review'></textarea>");
			var $newSpan = $("<span class='autor'></span>");
			$newReviewBlock.prependTo("#reviews");
			$newTextarea.prependTo($newReviewBlock);
			$newSpan.appendTo($newReviewBlock);
			
			$newTextarea.text(text);
			$newSpan.text(name);
			$("#reviews").scrollTop(0);
			ResizeSpan($newSpan);
			ResizeTextarea($newTextarea);
			AddMargin($newTextarea,$newReviewBlock);
		}
		);
		}
		else{
			alert("Вы звбыли добавить отзыв");
		}
			
	});
	
	$("#reviews").scroll(function(){
		
		var bottom = $(this)[0].scrollHeight - $(this)[0].clientHeight;
		
		
		if($(this).scrollTop() === bottom){
			$.post("../PHP/GetAdditionalReview.php",{},function(data){
				
				var $review_block = $("<div class='reviewBlock'></div>");
				var $textarea = $("<textarea readonly='readonly' cols='20' rows='4'  class='review' ></textarea>");
				var $span = $("<span class='autor'></span>");
				var $reviews = $("#reviews");
				data = JSON.parse(data);// ошибка здесь
				
				for(var j in data){
					
					var $clone = $review_block.clone();
					var $cloneTA = $textarea.clone();
					var $cloneS = $span.clone();
					
					$cloneTA.appendTo($clone);
					$cloneS.appendTo($clone);
					$clone.appendTo($reviews);
					
					$cloneS.text(data[j][0]);
					$cloneTA.text(data[j][1]);
					
					ResizeTextarea($cloneTA);
					ResizeSpan($cloneS);
					AddMargin($cloneTA,$clone);
				}
				
			});
	       }
	    });
});