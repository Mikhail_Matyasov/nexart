$(document).ready(function(){
	"use strict";
	var IsSearchFocus = 0;
	//------------------------------------------------
   $("#MainSearch input").focusin(function(){
	   IsSearchFocus = 1;
	   $(this).parent().css({
		   "border": "0.1em solid #F9C33E"
	   });
   });
   //------------------------------------------------
	$("#MainSearch input").focusout(function(){
	   $(this).parent().css({
		   "border": "0.1em solid #FFFFFF"
	   });
	   IsSearchFocus = 0;
   });
	
	
	//------------------------------------------------
	$("#MainSearch").mouseenter(function(){
		$(this).css({
			"border": "0.1em solid #F9C33E"
		});
	});
	//------------------------------------------------
	$("#MainSearch").mouseleave(function(){
		if(!IsSearchFocus){
			$(this).css({
				"border": "0.1em solid #FFFFFF"
			});
		}
	});
	//------------------------------------------------
	var headerIsFixed = 0;
	var time = 280;
	$(document).scroll(function(){
		var height = $(this).height();
		var $header = $("header");
		if($(this).scrollTop() > height/30){
			if(headerIsFixed === 0){
			  $header.animate({
				"opacity": "0.98",
				"height": "6vh",
			   },{
				   duration: time
			   });
				
			   $("#sections").animate({
				   "width": "40%",
	               "margin-left": "2%"
			   },{
				   duration: time
			   });
				
			   $("#search").animate({
				   "height": "80%",
	               "width": "15%",
	               "margin": "0.2% 2% 0 38%"
			   },{
				   duration: time
			   });
			}
			headerIsFixed = 1;
		}
		
		if($(this).scrollTop() === 0){
			if(headerIsFixed === 1){
				$header.animate({
				"opacity": "1",
				"height": "7vh"
			   },{
				   duration: time
			   });
				
			   $("#sections").animate({
				   "width": "70%",
	               "margin-left": "5%"
			   },{
				   duration: time
			   });
				
			   $("#search").animate({
				   "height": "100%",
	               "width": "23%",
	               "margin": "0% 2% 0 0"
			   },{
				   duration: time
			   });
			}
			headerIsFixed = 0;
		}
	});
	//--------------------------------------------
	$("header .section").mouseenter(function(){
		$(this).children().css({
			color: "#F9C33E"
		});
	});
	//--------------------------------------------
	$("header .section").mouseleave(function(){
		$(this).children().css({
			color: "white"
		});
	});
});