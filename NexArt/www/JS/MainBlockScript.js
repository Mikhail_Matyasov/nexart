$(document).ready(function(){
	"use strict";
	
	$("#articles ul li a").mouseenter(function(){
		
		$(this).css({
			"-webkit-transition": "all 0.1s ease",
			"-moz-transition": "all 0.1s ease",
			"-o-transition": "all 0.1s ease",
			"-ms-transition": "all 0.1s ease",
			"transition": "all 0.1s ease",
			color: "#F9C33E"
		});
		$(this).parent().parent("li").css({
			"-webkit-transition": "all 0.1s ease",
			"-moz-transition": "all 0.1s ease",
			"-o-transition": "all 0.1s ease",
			"-ms-transition": "all 0.1s ease",
			"transition": "all 0.1s ease",
			color: "#F9C33E"
		});
	});
	
	$("#articles ul li a").mouseleave(function(){
		
		$(this).css({
			"-webkit-transition": "all 0.1s ease",
			"-moz-transition": "all 0.1s ease",
			"-o-transition": "all 0.1s ease",
			"-ms-transition": "all 0.1s ease",
			"transition": "all 0.1s ease",
			color: "#2F2F2F"
		});
		$(this).parent().parent("li").css({
			"-webkit-transition": "all 0.1s ease",
			"-moz-transition": "all 0.1s ease",
			"-o-transition": "all 0.1s ease",
			"-ms-transition": "all 0.1s ease",
			"transition": "all 0.1s ease",
			color: "#2F2F2F"
		});
	});
    //--------------------------------------------
	setTimeout(function(){
		$(document).scrollTop(0);
	},10);
});