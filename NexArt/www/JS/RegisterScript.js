$(document).ready(function(){
	"use strict";
	
	$("input[type='text'],input[type='email'],input[type='password']").focusin(function(){
		
		$("#register input").each(function(){
		 if($(this).attr("data-error") === "yes"){
			$(this).val("");
			$(this).css({
				color: "black",
				"border-color": "#0F0F0F",
			    "background-color": "#BBBBBB",
				"box-shadow":"0 5px 7px rgba(0,0,0,0.8)"
			});
			$(this).attr("data-error","no");
		 }
		});
		
		$(this).css({
			"border-color": "#F9C33E",
			"background-color": "white"
		});
	});
	
	$("input[type='text'],input[type='email'],input[type='password']").focusout(function(){
		$(this).css({
			color: "inherit",
			"border-color": "#0F0F0F",
			"background-color": "#BBBBBB",
			"box-shadow":"0 5px 7px rgba(0,0,0,0.8)"
		});
		
	});
	
	function ToUpperFirstLetter(value){
		var left = value.substr(0,1).toUpperCase();
		var right = value.substr(1,value.length - 1).toLowerCase();
		return left + right;
	}
	
	$("#FIO input").focusout(function(){	
		$(this).val(ToUpperFirstLetter($(this).val()));
	});
	
	$("#register i").click(function(){
		var $input = $(this).siblings();
		$input.val("");
		$input.focus();
	});
	
	function WarningInput($input,text){
		$("#register input").each(function(){
			if($(this).attr("type") !== "button"){
				$(this).css({
					"border-color":"#0F0F0F",
					"background-color":"#BBBBBB",
					"color":"inherit",
					"box-shadow":"0 5px 7px rgba(0,0,0,0.8)"
				});
			}
		});
		
		
		var warning_color = "red";
		$input.attr("data-error","yes");
		$input.val(text);
		$input.css({
			"border-color":warning_color,
			"background-color":"white",
			"color":warning_color,
			"box-shadow": "0 5px 7px rgba(255,0,0,0.8)"
		});
	}
	
	function ErrorHandler(code_error){
		
		var $input;
		if(code_error === "empty_sName"){
			$input = $("#register .sName");
			WarningInput($input,"Введите фамилию");
		}
		else if(code_error === "empty_name"){
			$input = $("#register .name");
			WarningInput($input,"Введите имя");
		}
		else if(code_error === "incorrect_email"){
			$input = $("#register .email");
			WarningInput($input,"Не корректный Email");
		}
		else if(code_error === "incorrect_password"){
			$input = $("#register .password");
			$input.attr("type","text");
			$input.focusin(function(){
				$(this).attr("type","password");
			});
			WarningInput($input,"Не корректный пароль");
		}
		else if(code_error === "error_confirm"){
			$input = $("#register .confirmPassword");
			$input.attr("type","text");
			$input.focusin(function(){
				$(this).attr("type","password");
			});
			WarningInput($input,"Пароли не совпадают");
		}
	}
	//------------------------------------------------------------
	$("#register input[type='button']").click(function(){
		var sName,name,pName,email,password,confirmPassword;
		var code_error = "OK";
		$("#register input").each(function(){
			var class_name = $(this).attr("class");
	
			if($(this).attr("data-error") === "yes"){
				$(this).val("");
				$(this).attr("data-error","no");
			}
			
			
			var value = $(this).val();
//			if( class_name === "sName"){
//			   if(value === ""){
//				   code_error = "empty_sName";
//				   return false;
//			   }
//			   sName = $(this).val();
//			}
			if(class_name === "name"){
			   if(value === ""){
				  code_error = "empty_name";
				  return false;
			   }
			   name = $(this).val();
			}
//			else if(class_name === "pName"){
//				pName = $(this).val();
//			}
			else if(class_name === "email"){
			   var reg = RegExp("^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$");
			   if(!reg.test(value)){
				  code_error = "incorrect_email";
				  return false;
			   }
				email = $(this).val();
			}
			else if(class_name === "password"){
			   if(value === ""){
				  code_error = "incorrect_password";
				  return false;
			   }
				password = $(this).val();
			}
			else if(class_name === "confirmPassword"){
			   if(value !== password ){
				  code_error = "error_confirm";
				  return false;
			   }
				confirmPassword = $(this).val();
			}
		});
		if(code_error === "OK"){
				$.post("../PHP/SaveNewUser.php",{
					sName: sName,
					name: name,
					pName: pName,
					password: password,
					email: email

				},function(){
					document.location.href = "../index.php";
				});
		} else {
			ErrorHandler(code_error);
		}
	});
});