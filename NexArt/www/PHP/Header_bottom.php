<?php
 $fixed_header = "<header id='fixed_header'>".
	        "<div class='container_menu'>
			  <div class='portrait_menu'>
			    <i class='fa fa-times' aria-hidden='true'></i>".
				"<div id='portrait_sections'>".
					"<img class='logo' alt='logo' src='/NexArt_Logo.svg'>".
					"<div data-section='portrait-section' class='section'>
					  <span  class='defaultRef' href='".$path_AllArticles."' ></span><span>Все статьи</span>
					</div>".
//					"<div data-section='portrait-section' class='section'>
//					  <a class='defaultRef' href='/PHP/Register.php'><span>Регистрация</span></a>".
				    "</div>".
			   "</div>".
			  "</div>".
			"</div>".
	        "<div class='bar-container'><i class='fa fa-bars' aria-hidden='true'></i></div>".
			"<div itemscope itemtype='http://schema.org/SiteNavigationElement'  id='sections'>".
	            "<img class='logo' alt='logo' src='/NexArt_Logo.svg'>".
				"<div data-section='section' data-select='".$main_select."'  class='section'><span  class='defaultRef' href='".$path_AllArticles."' ></span><span>Все статьи</span></div>".
//				"<div data-section='section' data-select='".$reg_select."' class='section'><a class='defaultRef' href='/PHP/Register.php'><span>Регистрация</span></a>".
				"</div>".
			"</div>".
			"<div id='search'>".
//				"<div id='MainSearch'>".
//					"<input type='text' placeholder='Поиск'>".
//				"</div>".
			"</div>".
	        "<div class='container_font-size' data-font='size'>
			    <div class='info'>
				  <span>Шрифт</span>
				</div>
				<div class='input'>
				   <input type='text' value='50'>
				   <i class='fa fa-percent' aria-hidden='true'></i>
				</div>
			</div>".
		"</header>";

 $header = "</head>".
	"<body>".
		"<header  itemscope itemtype='http://schema.org/WPHeader'  id='defaultHeader'>".
	    "<meta itemprop='headline' content='".$title."'>
        <meta itemprop='description' content='".$discription."'>
        <meta itemprop='keywords' content='".$keywords."'>".
	        "<div class='container_menu'>
			  <div itemscope itemtype='http://schema.org/SiteNavigationElement' class='portrait_menu'>
			    <i class='fa fa-times' aria-hidden='true'></i>".
				"<div id='portrait_sections'>".
					"<img class='logo' alt='logo' src='/NexArt_Logo.svg'>".
					"<div data-section='portrait-section' class='section'>
					  <span itemprop='url' class='defaultRef' href='".$path_AllArticles."' ></span><span>Все статьи</span>
					</div>".
//					"<div data-section='portrait-section' class='section'>
//					  <a class='defaultRef' href='/PHP/Register.php'><span>Регистрация</span></a>".
				    "</div>".
			   "</div>".
			  "</div>".
			"</div>".
	        "<div class='bar-container'><i class='fa fa-bars' aria-hidden='true'></i></div>".
			"<div id='sections'>".
	            "<img class='logo' alt='logo' src='/NexArt_Logo.svg'>".
				"<div data-section='section' data-select='".$main_select."'  class='section'><span  class='defaultRef' href='".$path_AllArticles."' ></span><span>Все статьи</span></div>".
//				"<div data-section='section' data-select='".$reg_select."' class='section'><a class='defaultRef' href='/PHP/Register.php'><span>Регистрация</span></a>".
				"</div>".
			"</div>".
			"<div id='search'>".
//				"<div id='MainSearch'>".
//					"<input type='text' placeholder='Поиск'>".
//				"</div>".
			"</div>".
	        "<div class='container_font-size' data-font='size'>
			    <div class='info'>
				  <span>Шрифт</span>
				</div>
				<div class='input'>
				   <input type='text' value='50'>
				   <i class='fa fa-percent' aria-hidden='true'></i>
				</div>
			</div>".$fixed_header.
		"</header>";

 echo $header;
?>