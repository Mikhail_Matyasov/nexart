<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Регистрация</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
	<link rel="stylesheet" href="../CSS/RegisterStyle.css?v=1.0.6">
	<link rel="stylesheet" href="../CSS/Adap_Register.css?v=1.0.2">
	<script src="https://use.fontawesome.com/5399a799d4.js"></script>
</head>
<body>
	<div id="register">
		<div id="FIO">
<!--
			<div class="field">
				<input data-error="no" class="sName" placeholder="Фамилия" type="text">
				<i class="fa fa-minus-square" aria-hidden="true"></i>
			</div>
-->
			<div class="field">
				<input data-error="no" class="name" placeholder="Имя" type="text">
				<i data-portrait="no" class="fa fa-minus-square" aria-hidden="true"></i>
			</div>
<!--
			<div class="field">
				<input data-error="no" class="pName" placeholder="Отчество" type="text">
				<i class="fa fa-minus-square" aria-hidden="true"></i>
			</div>
-->
		</div>
		
		<div class="email_div">
			<div class="field">
				<input data-error="no" class="email" placeholder="Email" type="email">
				<i data-portrait="no" class="fa fa-minus-square" aria-hidden="true"></i>
			</div>
		</div>
		
		<div id="Password">
			<div class="field">
				<input data-error="no" class="password" placeholder="Введите пароль" type="password">
				<i data-portrait="no" class="fa fa-minus-square" aria-hidden="true"></i>
			</div>
			<div class="field">
				<input data-error="no" class="confirmPassword" placeholder="Повторите пароль" type="password">
				<i data-portrait="no" class="fa fa-minus-square" aria-hidden="true"></i>
			</div>
		</div>
		
		<input value="Зарегистрироваться" type="button">
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="../JS/RegisterScript.js?v=1.0.3"></script>
</body>
</html>