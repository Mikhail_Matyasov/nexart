<?php
 $scripts = "<link rel='stylesheet'".  "href='https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css'>".
  "<link rel='stylesheet' href='/CSS/GeneralStyle.css?v=1.0.20'>".
  "<link rel='stylesheet' href='/CSS/MainBlockStyle.css?v=1.0.23'>".
  "<link rel='stylesheet' href='/CSS/HeaderStyle.css?v=1.1.5'>".
  "<link rel='stylesheet' href='/CSS/FooterStyle.css?v=1.0.17'>".
  "<link rel='stylesheet' href='/CSS/Adaptability.css?v=1.2.13'>".
  "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>".
  "<script src='https://use.fontawesome.com/5399a799d4.js'></script>".
  "<script src='/JS/Adaptability.js?v=1.0.20'></script>".
  "<script src='/JS/HeaderScript.js?v=1.1.18'></script>".
  "<script src='/JS/MainBlockScript.js?v=1.0.5'></script>".
  "<script src='/JS/FooterScript.js?v=1.1.6'></script>";

echo $scripts;
?>