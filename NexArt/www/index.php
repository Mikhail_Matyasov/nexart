<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>NexArt</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
	<link rel="stylesheet" href="CSS/GeneralStyle.css?v=1.0.9">
	<link rel="stylesheet" href="CSS/MainBlockStyle.css?v=1.0.4">
	<link rel="stylesheet" href="CSS/HeaderStyle.css?v=1.0.7">
	<link rel="stylesheet" href="CSS/FooterStyle.css?v=1.0.11">
	<script src="https://use.fontawesome.com/5399a799d4.js"></script>
</head>
<body>
    <header id="defaultHeader">
		<div id="sections">
			<div class="section"><a class="defaultRef" href="PHP/All Articles.php"><span>Все статьи</span></a></div>
			<div class="section"><a class="defaultRef" href="PHP/Contact.php"><span>Контакты</span></a></div>
			<div class="section"><a class="defaultRef" href="PHP/All Articles.php"><span>Регистрация</span></a></div>
			<div class="section"><a class="defaultRef" href="PHP/All Articles.php"><span>Отзывы</span></a></div>
		</div>
		<div id="search">
			<div id="MainSearch">
				<input type="text" placeholder="Поиск">
			</div>
		</div>
	</header>
	<div id="MainBlock">
		<div id="articles">
			<ul>
				<li>
					<h1>
<!--
						<a href="#">
							Искусство быть богатым
						</a>
-->
				    </h1>
				</li>
				
			</ul>
		</div>
	</div>
	<footer>
		<div id="sendNewReview">
			<form>
				<div class="reviewBlock">
					<input placeholder="Имя:" name="name" type="text">
				    <i class="fa fa-minus-square" aria-hidden="true"></i>
				</div>
				<div class="reviewBlock">
				    <input placeholder="Email:" type="email" name="email">
				    <i class="fa fa-minus-square" aria-hidden="true"></i>
				</div>
				
				<div class="reviewBlock">
				    <textarea placeholder="Напишите отзыв" name="text" cols="30" rows="10"></textarea>
					<i class="fa fa-minus-square" aria-hidden="true"></i>
				</div>
				<input type="button" value="Отправить" name="submit">
			</form>
		</div>
		<div id="reviews">
			<?php require_once("PHP/GetReview.php")?>
		</div>
	</footer>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="JS/HeaderScript.js?v=1.0.3"></script>
	<script src="JS/MainBlockScript.js"></script>
	<script src="JS/FooterScript.js?v=1.0.18"></script>
</body>
</html>